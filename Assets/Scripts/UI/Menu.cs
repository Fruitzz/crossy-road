﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    private Vector3 startPosition;
#pragma warning disable 649
    [SerializeField] AudioSource audioSource;
    private void Start()
    {
        PlayAudio();
        StartCoroutine(GameStarted());
    }
    private IEnumerator GameStarted()
    {
        startPosition = transform.position;
        transform.DOMove(new Vector3(transform.position.x, transform.position.y + 50f, transform.position.z), 2, false);
        yield return new WaitForSeconds(2.2f);
        transform.DOMove(startPosition, 2, false);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Main");
    }
    private void PlayAudio()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
            audioSource.Play();
    }
}
