﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private Text score;
    void Update()
    {
       score.text = PlayerPrefs.GetInt("Score").ToString();
    }
}
