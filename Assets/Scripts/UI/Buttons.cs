﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject soundOff;
    [SerializeField] private GameObject soundOn;
    private void OnMouseDown()
    {
        transform.DOScale(70, 0.2f);
        if (PlayerPrefs.GetInt("Sound") == 1)
            audioSource.Play();
    }
    private void OnMouseUp()
    {
        if (gameObject.CompareTag("Button"))
            transform.DOScale(50, 0.2f);
    }
    private void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        {
            case "SoundOn":
                {
                    PlayerPrefs.SetInt("Sound", 0);
                    gameObject.SetActive(false);
                    transform.DOScale(50, 0.2f);
                    soundOff.SetActive(true);
                }
                break;
            case "SoundOff":
                {
                    PlayerPrefs.SetInt("Sound", 1);
                    gameObject.SetActive(false);
                    transform.DOScale(50, 0.2f);
                    soundOn.SetActive(true);
                }
                break;
            case "Play":
                {
                    SceneManager.LoadScene("Main");
                }
                break;
                    
        }
    }
}
