﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentNumbers : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private GameObject soundOff;
    [SerializeField] private GameObject soundOn;
    [SerializeField] private Text score;
    [SerializeField] private Text record;
    [SerializeField] private Text money;
    private void Start()
    {
        SetSound();
        SetNumbers();
    }
    private void SetSound()
    {
        DontDestroyOnLoad(GameObject.Find("AudioPlayer"));
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            soundOff.SetActive(false);
            soundOn.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("Sound") == 0)
        {
            soundOff.SetActive(true);
            soundOn.SetActive(false);
        }
    }
    private void SetNumbers()
    {
        score.text += PlayerPrefs.GetInt("Score").ToString();
        record.text += PlayerPrefs.GetInt("Record").ToString();
        money.text += PlayerPrefs.GetInt("Money").ToString();
    }
}
