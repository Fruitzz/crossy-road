﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Money : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private Text money;
    private void Update()
    {
       money.text = PlayerPrefs.GetInt("Money").ToString();
    }
}
