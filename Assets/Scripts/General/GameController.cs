﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private int maxCount;
#pragma warning disable 649
    [SerializeField] private int minDistance;

    private int safeCount;
    private List<string> possibleSafeGrounds;
    private Vector3 currentPosition;
    private List<GameObject> currentGrounds;
    private List<string> possibleNotSafeGrounds;
    private bool isStart;
    private void Start()
    {
        Init();
        for (int i = 0; i != maxCount; ++i)
        {
            if (i == 16)
                isStart = false;
            CreateGround(new Vector3(0, 0, 0));
        }
    }
    public void CreateGround(Vector3 player_position)
    {
        if (currentPosition.x - player_position.x < minDistance)
        {

            if (safeCount != 0 || isStart)
            {
                GetSafeGround();
            }

            else
            {
                int random = Random.Range(1, 4);
                while (safeCount < random)
                    GetNotSafeGround();
            }
            RemoveGround();
        }
    }
    private void Init()
    {
        isStart = true;
        safeCount = Random.Range(1, 4);
        possibleSafeGrounds = new List<string>() { "Ground1", "Ground2", "Ground3", "GroundL1", "GroundL2", "GroundL3" };
        possibleNotSafeGrounds = new List<string>() { "RoadRight", "RoadLeft", "WaterRight", "WaterLeft" };

        currentGrounds = new List<GameObject>();
        currentPosition = new Vector3(-10, 0, 0);
    }
    private void GetSafeGround ()
    {
        GameObject gameObject = PoolManager.GetObject(RandomGround(possibleSafeGrounds), currentPosition, Quaternion.identity);
        currentGrounds.Add(gameObject);
        currentPosition.x++;
        --safeCount;
        if (safeCount < 0)
            safeCount = 0;
    }

    private void GetNotSafeGround ()
    {
        GameObject gameObject = PoolManager.GetObject(RandomGround(possibleNotSafeGrounds), currentPosition, Quaternion.identity);
        currentGrounds.Add(gameObject);
        currentPosition.x++;
        ++safeCount;
    }

    private void RemoveGround()
    {
        if (currentGrounds.Count > maxCount)
        {
            currentGrounds[0].GetComponent<PoolObject>().ReturnToPool();
            currentGrounds.RemoveAt(0);
        }
    }

    private string RandomGround(List<string> grounds)
    {
        int random;
        random = Random.Range(0, grounds.Count);
        return grounds[random];
    }
}
