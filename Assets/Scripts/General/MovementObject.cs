﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementObject : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private float speed;
    public bool isRaft;
   private void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
