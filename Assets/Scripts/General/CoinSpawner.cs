﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    private void Start()
    {
        int rand = Random.Range(0, 2);
        Vector3 spawn = new Vector3(transform.position.x, 0.65f, transform.position.z + 0.25f);
        if(transform.childCount == 0)
            if (rand == 1)
            {
                GameObject coin = PoolManager.GetObject("Coin", spawn, Quaternion.identity);
                coin.transform.parent = gameObject.transform;
            }
    }
}
