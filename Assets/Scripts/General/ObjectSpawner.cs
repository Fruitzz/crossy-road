﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private List<GameObject> objects;
    [SerializeField] private Transform spawnPosition;
    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;
    [SerializeField] private bool isRight;

    private List<GameObject> currentObjects;
    private void Start()
    {
        currentObjects = new List<GameObject>();
        StartCoroutine(SpawnObject());
    }
    private IEnumerator SpawnObject ()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
            GameObject gameObject = PoolManager.GetObject(objects[Random.Range(0, objects.Count)].name, spawnPosition.position, Quaternion.identity);
            if (gameObject.transform.Find("Player"))
                gameObject.transform.GetChild(0).gameObject.GetComponent<PoolObject>().ReturnToPool();
            if(!isRight)
                gameObject.transform.Rotate(new Vector3(0, 180, 0));
            currentObjects.Add(gameObject);
            RemoveObject();
        }
    }
    private void RemoveObject ()
    {
        currentObjects.ForEach(el => 
        {
            if(Mathf.Abs(el.transform.position.z) > 16)
                el.GetComponent<PoolObject>().ReturnToPool();
        }
        );
        currentObjects.RemoveAll(el => Mathf.Abs(el.transform.position.z) > 16);
    }
}
