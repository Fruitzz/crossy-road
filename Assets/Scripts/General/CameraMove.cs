﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraMove : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private Player player;
    [SerializeField] private AudioSource audioSource;
    private void Start()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
            audioSource.Play();
    }
    private void Update()
    {
        if (player != null)
            transform.DOMove(player.transform.position, 0.1f, false);
    }
    public void SetPlayer(GameObject player)
    {
        this.player = player.GetComponent<Player>();
    }
    public void SetNull()
    {
        player = null;
    }
}
