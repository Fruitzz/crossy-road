﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Packages.Rider.Editor;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private bool isDragging, isMobilePlatform;
    private Vector2 tapPoint, swipeDelta;
    private float minSwipeDelta = 130;
    public enum SwipeType
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }
#pragma warning disable 649
    [SerializeField] public GameController controller;
    [SerializeField] public Text scoreText;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private Rigidbody rigidBody;
    [SerializeField] private PoolObject pool;
    [SerializeField] public CameraMove cameraMove;

    private double score;
    private float distance;
    private GameObject closestObject;
    private int backStep;
    private int money;

    private void Awake()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        isMobilePlatform = false;
#else
        isMobilePlatform = true;
#endif
    }

    private void Start()
    {
        backStep = 2;
        transform.position = new Vector3(5, 0.5f, 0);
        money = PlayerPrefs.GetInt("Money");
        distance = 0;
        closestObject = null;
        score = 0;
        PlayerPrefs.SetInt("Score", (int)score);
        scoreText.text = score.ToString();
    }
    private void Update()
    {
        CheckInput();
        StartCoroutine(SpawnEagle());
        CheckPossibleDistance();
        ScoreUpdate();
    }
    private void Movement(Vector3 distanse, Vector3 rotation)
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
            audioSource.Play();
        if (transform.parent)
            transform.parent = null;
        RestorePosition();
        rigidBody.DOJump(transform.position + distanse, 1, 1, 0.2f, false);
        transform.DORotate(rotation, 0.1f, RotateMode.Fast);

        controller.CreateGround(transform.position);

    }
    private void ScoreUpdate()
    {
        if (score < Math.Round(transform.position.x - 5))
        {
            score = Math.Round(transform.position.x - 5);
            PlayerPrefs.SetInt("Score", (int)score);
            scoreText.text = PlayerPrefs.GetInt("Score").ToString();
        }
    }
    private void RestorePosition()
    {
        if (transform.position.y > 0.5f)
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
    }
    private void ChooseDirection(SwipeType type) // !!Я знаю, что это абсолютно ужасно, но я правда не придумал способа лучше
    {

        if (closestObject)
        {
            Vector3 temp = transform.position;
            if (type == SwipeType.UP)
            {
                if (Vector3.Distance(temp + new Vector3(1, 0, 0), closestObject.transform.position) > distance)
                {
                    Movement(new Vector3(1, 0, 0), new Vector3(0, 90, 0));
                    backStep = 2;
                }
                else
                    Movement(new Vector3(0, 0, 0), new Vector3(0, 90, 0));

            }
            else if (type == SwipeType.LEFT)
            {
                if (Vector3.Distance(temp + new Vector3(0, 0, 1), closestObject.transform.position) > distance)
                {
                    Movement(new Vector3(0, 0, 1), new Vector3(0, 0, 0));
                }
                else
                    Movement(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
            }
            else if (type == SwipeType.RIGHT)
            {
                if (Vector3.Distance(temp + new Vector3(0, 0, -1), closestObject.transform.position) > distance)
                {
                    Movement(new Vector3(0, 0, -1), new Vector3(0, 180, 0));
                }
                else
                    Movement(new Vector3(0, 0, 0), new Vector3(0, 180, 0));
            }
            else if (type == SwipeType.DOWN)
            {
                if (Vector3.Distance(temp + new Vector3(-1, 0, 0), closestObject.transform.position) > distance)
                {
                    Movement(new Vector3(-1, 0, 0), new Vector3(0, 270, 0));
                    backStep--;
                }
                else
                    Movement(new Vector3(0, 0, 0), new Vector3(0, 270, 0));

            }
        }
        else if (type == SwipeType.UP)
        {
            Movement(new Vector3(1, 0, 0), new Vector3(0, 90, 0));
            backStep = 2;
        }
        else if (type == SwipeType.LEFT)
            Movement(new Vector3(0, 0, 1), new Vector3(0, 0, 0));
        else if (type == SwipeType.RIGHT)
            Movement(new Vector3(0, 0, -1), new Vector3(0, 180, 0));
        else if (type == SwipeType.DOWN)
        {
            Movement(new Vector3(-1, 0, 0), new Vector3(0, 270, 0));
            backStep--;
        }
    }

    private IEnumerator SpawnEagle()
    {
        if (backStep == 0)
        {
            backStep = 2;
            GameObject eagle = PoolManager.GetObject("Eagle", new Vector3(transform.position.x + 8, transform.position.y + 1, transform.position.z), Quaternion.identity);
            eagle.transform.DORotate(new Vector3(0, -90, 0), 0);
            eagle.transform.DOMove(new Vector3(transform.position.x - 10, transform.position.y, transform.position.z), 1.4f);
            if (PlayerPrefs.GetInt("Sound") == 1)
                eagle.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.5f);
            pool.ReturnToPool();
            SceneManager.LoadScene("GameOver");
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Raft"))
        {
                transform.parent = collision.gameObject.transform;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("StaticObject"))
        {
            distance = Vector3.Distance(transform.position, other.gameObject.transform.position);
            closestObject = other.gameObject;
        }
       else if (other.gameObject.CompareTag("Coin"))
        {
            StartCoroutine(other.gameObject.GetComponent<Coin>().Picked());
            money++;
            MoneyUpdate();
        }
      // else if (other.gameObject.CompareTag("Killer"))
            //StartCoroutine(Death());
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("StaticObject"))
        {
            distance = 0;
            closestObject = null;
        }
    }
    private void MoneyUpdate()
    {
        PlayerPrefs.SetInt("Money", money);
    }
    private void CheckPossibleDistance()
    {
       // if ((gameObject.transform.position.z > 5.7f || gameObject.transform.position.z < -8))
           // StartCoroutine(Death());
    }
    public IEnumerator Death()
    {
        cameraMove.SetNull();
        transform.DOJump(gameObject.transform.position, 2, 1, 2);
        transform.DORotate(new Vector3(0, 0, 180), 2);
        yield return new WaitForSeconds(1);
        pool.ReturnToPool();
        CheckForRecord();
        SceneManager.LoadScene("GameOver");
    }
    private void CheckForRecord()
    {
        if (score > PlayerPrefs.GetInt("Record"))
            PlayerPrefs.SetInt("Record", (int)score);
    }
    ////
    private void CheckInput()
    {
        if (!isMobilePlatform)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isDragging = true;
                tapPoint = Input.mousePosition;
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                {
                    isDragging = true;
                    tapPoint = Input.touches[0].position;
                }
                else if (Input.touches[0].phase == TouchPhase.Canceled || Input.touches[0].phase == TouchPhase.Ended)
                    ResetSwipe();
            }
        }
        CalculateSwipe();
    }
    private void CalculateSwipe()
    {
        swipeDelta = Vector2.zero;
        if (isDragging)
        {
            if (!isMobilePlatform && Input.GetMouseButton(0))
                swipeDelta = (Vector2)Input.mousePosition - tapPoint;
            else if (Input.touchCount > 0)
                swipeDelta = Input.touches[0].position - tapPoint;
        }
        if (swipeDelta.magnitude > minSwipeDelta)
        {
            if (Mathf.Abs(swipeDelta.x) > Math.Abs(swipeDelta.y))
                ChooseDirection(swipeDelta.x < 0 ? SwipeType.LEFT : SwipeType.RIGHT);
            else
                ChooseDirection(swipeDelta.y > 0 ? SwipeType.UP : SwipeType.DOWN);
            ResetSwipe();
        }
    }
    private void ResetSwipe()
    {
        isDragging = false;
        tapPoint = swipeDelta = Vector2.zero;
    }
}
