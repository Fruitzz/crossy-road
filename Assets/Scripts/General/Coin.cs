﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Coin : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] PoolObject pool; 
   public IEnumerator Picked ()
    {
        gameObject.transform.DOJump(new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z), 2, 1, 1);
        yield return new WaitForSeconds(0.5f);
        pool.ReturnToPool();
    }
}
